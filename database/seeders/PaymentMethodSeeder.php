<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Initial value to variable
        $payments = ["Cash", "Transfer", "Credit Card"];

        foreach ($payments as $value) {

            // Check if exist
            $checkPayment = PaymentMethod::where('payment_name', $value)->first();

            // Create new record for payment if not exist
            if (!$checkPayment) {
                $payment = new PaymentMethod();
                $payment->payment_name = $value;
                $payment->save();
            }
        }
    }
}
