<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name' => 'Product 1',
                'description' => 'Description Product 1',
                'price' => 10000,
                'qty' => 100
            ],
            [
                'name' => 'Product 2',
                'description' => 'Description Product 2',
                'price' => 20000,
                'qty' => 100
            ],
            [
                'name' => 'Product 3',
                'description' => 'Description Product 3',
                'price' => 20000,
                'qty' => 100
            ]
        ];

        $categories = ProductCategory::all();

        // Looping category
        foreach ($categories as $category) {
            // Looping products
            foreach ($products as $product) {
                $product_name = $product['name'] ." ". $category->category_name;
                $product_desc = $product['description'] ." ". $category->category_name;
                $product_price = $product['price'];
                $product_qty = $product['qty'];

                // Check if product exist
                $checkProduct = Product::where('category_id', $category->id)->where('product_name', $product_name)->first();

                // Create new record for product if not exist
                if (!$checkProduct) {
                    $product = new Product();
                    $product->category_id = $category->id;
                    $product->product_name = $product_name;
                    $product->description = $product_desc;
                    $product->price = $product_price;
                    $product->qty = $product_qty;
                    $product->save();
                }
            }
        }
    }
}
