<?php

namespace Database\Seeders;

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Initial value to variable
        $categories = ["Food", "Fashion", "Electronic"];

        foreach ($categories as $value) {

            // Check if exist
            $checkCategory = ProductCategory::where('category_name', $value)->first();

            // Create new record for category if not exist
            if (!$checkCategory) {
                $category = new ProductCategory();
                $category->category_name = $value;
                $category->save();
            }
        }
    }
}
