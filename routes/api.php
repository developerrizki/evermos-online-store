<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\v1\AuthController;
use App\Http\Controllers\v1\PaymentController;
use App\Http\Controllers\v1\ProductController;
use App\Http\Controllers\v1\ProfileController;
use App\Http\Controllers\v1\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function(){
    Route::group(['middleware' => 'auth:sanctum'], function(){
        // Route Profile
        Route::get('/profile', [ProfileController::class, 'index']);
        Route::put('/profile', [ProfileController::class, 'store']);

        // Route Product Cart
        Route::get('/product-carts', [ProductController::class, 'listCart']);
        Route::post('/product-cart', [ProductController::class, 'cart']);

        // Route Transaction
        Route::get('/transactions', [TransactionController::class, 'index']);
        Route::get('/transaction/{id}', [TransactionController::class, 'show']);
        Route::post('/transaction', [TransactionController::class, 'store']);
    });

    //Route Login
    Route::post('/login', [AuthController::class, 'login']);

    //Route Product
    Route::get('/products', [ProductController::class, 'index']);
    Route::get('/product/{id}', [ProductController::class, 'show']);

    //Route Payment
    Route::get('/payment-methods', [PaymentController::class, 'index']);
});
