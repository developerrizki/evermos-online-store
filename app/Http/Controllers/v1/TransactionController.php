<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductStockHistory;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        try {
            $user = $request->user();

            $transactions = Transaction::where('user_id', $user->id)->get();

            // Response success
            return response()->json([
                'status' => 'success',
                'message' => 'Get Transaction is successfully',
                'data' => $transactions
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function show($id)
    {
        $transaction = Transaction::with('details')->findOrFail($id);

        // Response success
        return response()->json([
            'status' => 'success',
            'message' => 'Get Transaction detail is successfully',
            'data' => $transaction
        ]);
    }

    public function store(Request $request)
    {
        try {
            $user = $request->user();

            $payment_id = $request->payment_id;
            $products = $request->products;

            // check available stock
            $this->checkAvailable($products);

            DB::beginTransaction();

                $transaction = new Transaction();
                $transaction->code = "TRX-EVRMS-". date("dmYHis") . "-" . strtoupper(Str::random(5));
                $transaction->date = date('Y-m-d');
                $transaction->status = "Pending";
                $transaction->user_id = $user->id;
                $transaction->payment_id = $payment_id;
                $transaction->save();

                // process add to table product_stock_histories
                $this->addToHistory($products, $transaction);

                // process add to table transaction_details
                $this->addToDetail($products, $transaction);

            DB::commit();

            // Response success
            return response()->json([
                'status' => 'success',
                'message' => 'Create Transaction is successfully',
                'data' => $transaction
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    private function checkAvailable($products)
    {
        foreach ($products as $item) {
            $product = Product::find($item['product_id']);

            if ($product->qty < $item['qty']) {
                throw new \Exception('This product ' . $product->product_name . ' is not avaliable');
            }
        }

        return true;
    }

    private function addToHistory($products, $transaction)
    {
        foreach ($products as $item) {
            DB::beginTransaction();
                // Get data product
                $product = Product::find($item['product_id']);

                // Process add to history
                $history = new ProductStockHistory();
                $history->product_id = $item['product_id'];
                $history->transaction_id = $transaction['id'];
                $history->qty_prev = $product->qty;
                $history->qty_current = $product->qty - $item['qty'];
                $history->save();

                // Process update stock
                $product->qty = $product->qty - $item['qty'];
                $product->save();
            DB::commit();
        }

        return true;
    }

    private function addToDetail($products, $transaction)
    {
        foreach ($products as $item) {
            // Get data product
            $product = Product::find($item['product_id']);

            // Insert new record to detail transaction
            $detail = new TransactionDetail();
            $detail->transaction_id = $transaction->id;
            $detail->product_id = $item['product_id'];
            $detail->qty = $item['qty'];
            $detail->price = $product->price;
            $detail->save();
        }

        return true;
    }
}
