<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        // Check user with username
        $user = User::where('username', $request->username)->first();

        // Check password
        if(!$user || !Hash::check($request->password, $user->password)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'
            ],401);
        }

        // Add token from Laravel Sanctum
        $user->token = $user->createToken($user->email)->plainTextToken;

        // Response
        return response()->json([
            'status' => 'success',
            'message' => 'Login Successfully',
            'data' => $user
        ]);
    }
}
