<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCart;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        // Get data product with category
        return response()->json([
            'status' => 'success',
            'message' => 'Get Products Successfully',
            'data' => $products->pluck('response')
        ]);
    }

    public function show($id)
    {
        $product = Product::find($id);

        // Get data product with category
        return response()->json([
            'status' => 'success',
            'message' => 'Get Product Detail Successfully',
            'data' => $product->response_detail
        ]);
    }

    public function cart(Request $request)
    {
        try {
            $user = $request->user();
            $payloads = $request->all();

            foreach ($payloads as $item) {
                // check cart if exist
                $checkCart = ProductCart::where('user_id', $user->id)->where('product_id', $item['product_id'])->first();

                // if exist
                if ($checkCart) {
                    $cart = ProductCart::find($checkCart->id);
                    $cart->qty = $checkCart->qty + $item['qty'];
                    $cart->save();
                } else {
                    $cart = new ProductCart();
                    $cart->user_id = $user->id;
                    $cart->product_id = $item['product_id'];
                    $cart->qty = $item['qty'];
                    $cart->save();
                }
            }

            // Response success
            return response()->json([
                'status' => 'success',
                'message' => 'Update Product Cart Successfully'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function listCart(Request $request)
    {
        try {
            $user = $request->user();

            $carts = ProductCart::where('user_id', $user->id)->get();

            // Response success
            return response()->json([
                'status' => 'success',
                'message' => 'Get Product Cart Successfully',
                'data' => $carts->pluck('response')
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
