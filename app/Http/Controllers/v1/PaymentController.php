<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = PaymentMethod::all();

        // Get data product with category
        return response()->json([
            'status' => 'success',
            'message' => 'Get Payment Successfully',
            'data' => $payments->pluck('response')
        ]);
    }
}
