<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        // Get data profile user
        return response()->json([
            'status' => 'success',
            'message' => 'Get Profile Successfully',
            'data' => $request->user()
        ]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
                // process update user
                $user = $request->user();
                $user->username = $request->username;
                $user->email = $request->email;
                $user->name = $request->name;

                if ($request->has('password') && $request->password != "") {
                    $user->password = bcrypt($request->password);
                }

                $user->save();

                // process add to customer
                $checkCustomer = Customer::where('user_id', $user->id)->first();

                // if customer if exist
                if ($checkCustomer) {
                    $checkCustomer->address = $request->address;
                    $checkCustomer->phone = $request->phone;
                    $checkCustomer->save();
                } else {
                    $customer = new Customer();
                    $customer->user_id = $user->id;
                    $customer->customer_name = $user->name;
                    $customer->address = $request->address;
                    $customer->phone = $request->phone;
                    $customer->save();
                }

            DB::commit();

            // Response success
            return response()->json([
                'status' => 'success',
                'message' => 'Update profile is successfully'
            ]);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
