<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    /**
     * Get the category that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function getResponseAttribute()
    {
        return [
            'id' => $this->id,
            'name' => $this->product_name,
            'price' => $this->price,
            'qty' => $this->qty,
            'category' => optional($this->category)->response
        ];
    }

    public function getResponseDetailAttribute()
    {
        return [
            'id' => $this->id,
            'name' => $this->product_name,
            'description' => $this->description,
            'price' => $this->price,
            'qty' => $this->qty,
            'category' => optional($this->category)->response
        ];
    }
}
