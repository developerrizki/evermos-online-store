<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    protected $table = "product_categories";

    public function getResponseAttribute()
    {
        return [
            'id' => $this->id,
            'name' => $this->category_name
        ];
    }
}
