## Describe and give solution about problem on 12.12

- After I read and analyzed the problem regarding flash sale on 12.12, the main problem is about stock products included in the flash sale. The stock is not real time to minus every product go to checkout and pay. So I have the conclusion/solution we need to process real time for product stock.

## How To Run This Project

- create file .env base from .env.example  
- setup your local database add to .env
- run : php artisan key:generate
- run : php artisan config:cache
- run : php artisan migrate
- run : php artisan db:seed
- run : php artisan serve
- Go to : http://localhost:8000/